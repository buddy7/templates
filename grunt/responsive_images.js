// responsive images
// grunt-responsive-images: <https://github.com/andismith/grunt-responsive-images>

'use strict';

module.exports = {

    tmp: {
        options: {
            sizes: [{
                name: 's',
                width: '50%'
            }, {
                name: 'm',
                width: '100%'
            }, {
                upscale: true,
                name: 'l',
                width: '200%',
                separator: '-',
                suffix: '_x2'
            }]
        },
        files: [{
            expand: true,
            cwd: '<%= path.images %>',
            dest: '<%= path.tmpDist %>/img',
            src: '**/*.{png,jpg,gif}'
        }]
    },

    dist: {
        options: {
            sizes: [{
                name: 's',
                width: '50%'
            }, {
                name: 'm',
                width: '100%'
            }, {
                upscale: true,
                name: 'l',
                width: '200%',
                separator: '-',
                suffix: '_x2'
            }]
        },
        files: [{
            expand: true,
            cwd: '<%= path.images %>',
            dest: '<%= path.dist %>/img',
            src: '**/*.{png,jpg,gif}'
        }]
    }

};
