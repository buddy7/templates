// Grunt task registration
// Manage grunt tasks here instead of `grunt.registerTask`.
// Refer: <https://github.com/firstandthird/load-grunt-config#aliases>

'use strict';

var grunt = require('grunt');

module.exports = {

    // Generate source codes and spritesheet images
    compile: [
        'clean:tmp',
        'wiredep',
        'sprite',
        'stylus',
        'responsive_images:tmp',
        'autoprefixer'
    ],

    // Validation and lint
    lint: function (target) {
        if (target !== 'skip-compile') {
            grunt.task.run([
                'compile'
            ]);
        }
        grunt.task.run([
            'newer:validation',
            'newer:csslint',
            'newer:jshint'
        ]);
    },

    // Start localhost server
    serve: function (target) {
        if (target === 'dist') {
            grunt.task.run([
                'connect:dist'
            ]);
        }
        else {
            grunt.task.run([
                'compile',
                'ect:tmp',
                'ect:compile',
                'ssi',
                'connect:src',
                'esteWatch'
            ]);
        }
    },

    // Distribute. This is a part of build task.
    _dist: [
        'clean:dist',
        'copy:dist',
        'responsive_images:dist',
        'imagemin',
        'cmq',
        'useminPrepare',
        'concat',
        'cssmin',
        'uglify',
        // 'rev',
        'usemin'
    ],

    // Build
    build: [
        'compile',
        '_dist'
    ],

    // Preview on kakunin
    preview: [
        'compile',
        '_dist',
        'clean:preview',
        'copy:preview',
        'relativeRoot',
        'ftp-deploy'
    ],

    // Define default `grunt` alias
    default: [
        'compile',
        'lint:skip-compile',
        '_dist'
    ]

};
