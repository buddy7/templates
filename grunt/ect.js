// ECT Templates
// grunt-ect: <https://bitbucket.org/2no/grunt-ect>

'use strict';

var grunt = require('grunt');

module.exports = {

    options: {
        // please comment out the following 2 lines if you want to change the ect's opening / closing tags
        // please refer to the bitbucket page for various opening / closing tags
        // open: '{{',
        // close: '}}'
    },

    // Compile markups
    tmp: {
        options: {
            root: '<%= path.src %>',
            // Define global variables here or read JSON file.
            variables: {
                data: grunt.file.readJSON('src/_data/variables.json')
            }
        },
        expand: true,
        cwd: '<%= path.src %>/_template',
        src: [
            '**/*.ect',
            '!**/_*.ect'
        ],
        dest: '<%= path.tmpDist %>',
        ext: '.html'
    },

    compile: {
        options: {
            root: '<%= path.src %>',
            // Define global variables here or read JSON file.
            variables: {
                data: grunt.file.readJSON('src/_data/variables.json')
            }
        },
        expand: true,
        cwd: '<%= path.src %>/_template',
        src: [
            '**/*.ect',
            '!**/_*.ect'
        ],
        dest: '<%= path.html %>',
        ext: '.html'
    }

};
