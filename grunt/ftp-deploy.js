// ftp deploy
// grunt-ftp-deploy: <https://github.com/zonak/grunt-ftp-deploy>

'use strict';

module.exports = {

    preview: {
        auth: {
            host: 'teamsite01.rakuten.co.jp',    // Please specify hostname
            port: 21,
            authKey: 'kakunin'          // arbitrary value is okay
        },
        src: '<%= path.preview %>',
        dest: '/default/main/kakunin.rakuten/all/WORKAREA/00-PUBLIC/htdocs/rakuten/swd/ichiba/discovery'         // A folder which the files will be transferred
    }

};
