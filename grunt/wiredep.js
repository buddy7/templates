// Inject Bower components
// grunt-wiredep: <https://github.com/stephenplusplus/grunt-wiredep>
// wiredep: <https://github.com/taptapship/wiredep>

'use strict';

module.exports = {

    // Targets for auto injection
    target: {
        src: ['<%= path.markups %>/**/*.html'],
        // Force absolute URL
        fileTypes: {
            html: {
                replace: {
                    js: '<script src="/{{filePath}}"></script>',
                    css: '<link rel="stylesheet" href="/{{filePath}}">'
                }
            }
        },
        // Remove unnecessary path: "/../bower_components/xxx" -> "/bower_components/xxx"
        ignorePath: /(\.\.\/)+/
    }

};
