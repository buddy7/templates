// Watch updates and run predefined tasks
// grunt-este-watch: <https://github.com/steida/grunt-este-watchh>

'use strict';

module.exports = function(grunt, config) {
    return {
        options: {
            dirs: [
                config.path.src + '/**/',
                config.path.styles + '/**/', // for styl task
                config.path.tmp + '/**/', // for css and ect and ssi task
                config.path.templates + '/**/', // for ect task
                config.path.markups + '/**/'
            ],
            livereload: {
                enabled: true,
                extensions: ['html', 'css'],
                port: 35729
            }
        },

        // Stylus compile
        styl: function() {
            return ['stylus'];
        },

        // Autoprefix CSS
        css: function() {
            return ['newer:autoprefixer'];
        },

        // Ect compile
        ect: function() {
            return ['ect'];
        },

        // SSI
        ssi: function() {
            return ['ssi'];
        }
    };
};
