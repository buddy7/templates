'use strict';

module.exports = {
    preview: {
        options: {
            root: '<%= path.preview %>'
        },
        files: [{
            expand: true,
            cwd: '<%= relativeRoot.preview.options.root %>',
            src: ['**/*.html', '**/*.css'],
            dest: '<%= path.preview %>'
        }]
    }
};
