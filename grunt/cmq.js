// combine media queries
// grunt-combine-media-queries: <https://github.com/buildingblocks/grunt-combine-media-queries>

'use strict';

module.exports = {

    target: {
        files: {
            '<%= path.css %>': ['<%= path.css %>/**/*.css']
        }
    }

};
