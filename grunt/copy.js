// Copy files and folders
// grunt-contrib-copy: <https://github.com/gruntjs/grunt-contrib-copy>

'use strict';

module.exports = {

    // Distribution
    dist: {
        files: [
            // Copy static files.
            {
                expand: true,
                dot: true,
                cwd: '<%= path.src %>',
                dest: '<%= path.dist %>',
                filter: 'isFile',               // Ignore blank directory.
                src: [
                    '**',
                    '!<%= path.distIgnore %>',
                    '!bower_components/**',
                    '!**/*.{css,js}',           // Ignore CSS and JavaScript because they are compiled in usemin task.
                    '!**/*.{png,jpg,gif}'       // Ignore images because they are copied in imagemin task.
                ]
            }
        ]
    },

    preview: {
        files: [{
            expand: true,
            cwd: '<%= path.dist %>',
            dest: '<%= path.preview %>',
            src: [
                '**',
                '**/*.{css,js,jpg,gif,png}',
            ]
        }]
    }

};
