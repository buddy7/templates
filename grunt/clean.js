// Clean files and folders
// grunt-contrib-clean: <https://github.com/gruntjs/grunt-contrib-clean>

'use strict';

module.exports = {

    // Distribution directory
    dist: ['<%= path.dist %>/*'],

    // Temporary compiled files
    tmp: ['<%= path.tmpDist %>/*'],

    // Preview files
    preview: ['<%= path.preview %>/**']

};
