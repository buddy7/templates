// Connect web server
// grunt-contrib-connect: <https://github.com/gruntjs/grunt-contrib-connect>
// Connect: <https://github.com/senchalabs/connect>

'use strict';

module.exports = {

    // Base options
    options: {
        hostname: '0.0.0.0'                 // Change this to 'localhost' to prevent access from outside.
    },

    // Dev server with live-reload
    src: {
        options: {
            port: 9000,
            open: 'http://localhost:9000/', // Set Boolean or specific URL you want to open.
            base: [
                '<%= path.tmpDist %>',      // Refer compiled files at first,
                '<%= path.src %>'           // And then, look for other files in source direcotry.
            ],
            livereload: 35729               // Set `true` or port number to inject livereload.js.
        }
    },

    // Server using distribution files
    dist: {
        options: {
            port: 9001,
            open: 'http://localhost:9001/',
            base: [
                '<%= path.dist %>'
            ],
            keepalive: true
        }
    }

};
