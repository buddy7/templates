// Watch updates and run predefined tasks
// grunt-contrib-watch: <https://github.com/gruntjs/grunt-contrib-watch>

'use strict';

module.exports = {

    // Live reload server
    server: {
        files: [
            '<%= path.src %>/**',
            '<%= path.tmpDist %>/**',
            '!<%= path.distIgnore %>'
        ],
        options: {
            livereload: '<%= connect.src.options.livereload %>'
        }
    },

    // Stylus compile
    stylus: {
        files: ['<%= path.styles %>/**'],
        tasks: ['stylus']
    },

    // Autoprefix CSS
    autoprefixer: {
        files: ['<%= path.css %>/**'],
        tasks: ['newer:autoprefixer']
    },

    // SSI emulation
    ssi: {
        files: ['<%= path.markups %>/**/*.html'],
        tasks: ['ssi']
    }

};
